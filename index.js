const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const exerciseRouter = require("./routes/exercises");
const userRouter = require("./routes/users");
const morgan = require("morgan");
const path = require("path");

const config = require("./config.js");
// // configure .env file
// require("dotenv").config();

// create express server
const app = express();
// const port = process.env.PORT || 5000;

// middlewares
app.use(cors());
app.use(express.json());

// Database Connection to MongoAtlas
const uri = config.db;
mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
});

const connection = mongoose.connection;
connection.once("open", () => {
    console.log("MongoDB database connection established sucessfully");
});

app.use(morgan("tiny"));

app.use("/static", express.static(path.join(__dirname, "client/build/static")));

app.use(
    "/manifest.json",
    express.static(path.join(__dirname, "client/build/manifest.json"))
);

app.use(
    "/logo192.png",
    express.static(path.join(__dirname, "client/build/logo192.png"))
);

app.use(
    "/favicon.ico",
    express.static(path.join(__dirname, "client/build/favicon.ico"))
);

app.use("/api/exercises", exerciseRouter);
app.use("/api/users", userRouter);

// Set static folder
app.use(express.static(path.join(__dirname, "client/build")));

// server listener
app.listen(config.port, () => {
    console.log(`Server is running on port: ${config.port}`);
});