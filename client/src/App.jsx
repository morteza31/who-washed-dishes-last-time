import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { useAlert } from "react-alert";
import Navbar from "./components/navbar.component.jsx";
import ExercisesList from "./components/exercises-list.component";
import EditExercise from "./components/edit-exercise.component";
import CreateExercise from "./components/create-exercise.component";
import CreateUser from "./components/create-user.component";
// import logo from "./logo.svg";
import "./App.css";

function App() {
	const alert = useAlert() || null;
	return (
		<Router>
			<div className="container">
				<Navbar />
				<br />
				<Route
					path="/"
					exact
					render={(props) => <ExercisesList {...props} alert={alert} />}
				/>
				<Route
					path="/edit/:id"
					render={(props) => <EditExercise {...props} alert={alert} />}
				/>
				<Route
					path="/create"
					render={(props) => <CreateExercise {...props} alert={alert} />}
				/>
				<Route path="/user" component={CreateUser} />
			</div>
		</Router>
	);
}

export default App;
