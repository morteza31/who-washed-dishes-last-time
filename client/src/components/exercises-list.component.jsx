import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const Exercise = (props) => (
	<tr>
		<td>{props.exercise.username}</td>
		<td>{props.exercise.description}</td>
		<td>{props.exercise.duration}</td>
		<td>{props.exercise.date.substring(0, 10)}</td>
		<td>
			<Link to={"/edit/" + props.exercise._id}>edit</Link> |{" "}
			<a
				href="#"
				onClick={() => {
					props.deleteExercise(props.exercise._id);
				}}>
				delete
			</a>
		</td>
	</tr>
);

export default class ExercisesList extends Component {
	constructor(props) {
		super(props);

		this.deleteExercise = this.deleteExercise.bind(this);

		this.state = { exercises: [], loading: false };
	}

	componentDidMount() {
		console.log("Did mount Called!");
		axios
			.get("/api/exercises/")
			.then((response) => {
				this.setState({ exercises: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
		this.setState({ loading: false });
	}

	deleteExercise(id) {
		axios({
			method: "delete",
			url: "/api/exercises/",
			headers: {},
			data: {
				id: id,
			},
		}).then((res) => {
			this.props.alert.show(res.data);
			this.props.history.push("/", {
				state: false,
			});
		});

		this.setState({
			exercises: this.state.exercises.filter((el) => el._id !== id),
		});
	}

	exerciseList() {
		return this.state.exercises.map((currentexercise) => {
			return (
				<Exercise
					exercise={currentexercise}
					deleteExercise={this.deleteExercise}
					key={currentexercise._id}
				/>
			);
		});
	}

	render() {
		return (
			<div>
				<h3>Washed Dishes</h3>
				<table className="table table-dark">
					<thead className="thead-light">
						<tr>
							<th>Username</th>
							<th>Ate food with</th>
							<th>It took (Minutes)</th>
							<th>Date</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>{this.exerciseList()}</tbody>
				</table>
			</div>
		);
	}
}
