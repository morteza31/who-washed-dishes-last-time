import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-oldschool-dark";

ReactDOM.render(
	<React.StrictMode>
		<AlertProvider template={AlertTemplate}>
			<App />
		</AlertProvider>
	</React.StrictMode>,
	document.getElementById("root")
);

serviceWorker.unregister();
