const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const exerciseSchema = new Schema({
    username: { type: String, equired: true },
    description: { type: String, equired: true },
    duration: { type: Number, equired: true },
    date: { type: Date, equired: true },
}, {
    timestamps: true,
});

const Exercise = mongoose.model("Exercise", exerciseSchema);

module.exports = Exercise;