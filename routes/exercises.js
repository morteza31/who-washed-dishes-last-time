const router = require("express").Router();
let Exercise = require("../models/exercise.model");

router.route("/").get((req, res) => {
    Exercise.find()
        .then((exercises) => res.json(exercises))
        .catch((err) => res.status(400).json("Error: " + err));
});

router.route("/").post((req, res) => {
    if (req.body.id) {
        Exercise.findById(req.body.id)
            .then((exercise) => res.json(exercise))
            .catch((err) => res.status(400).json("Error: " + err));
    } else {
        const username = req.body.username;
        const description = req.body.description;
        const duration = Number(req.body.duration);
        const date = Date(req.body.date);
        const newExercise = new Exercise({
            username,
            description,
            duration,
            date,
        });

        newExercise
            .save()
            .then(() => res.json("Dish Washing Record Added!"))
            .catch((err) => res.status(400).json("Error: " + err));
    }
});

router.route("/").delete((req, res) => {
    Exercise.findByIdAndDelete(req.body.id)
        .then(() => res.json("Dish Washing Record Deleted."))
        .catch((err) => res.status(400).json("Error: " + err));

    console.log(`DishWashing with ID: ${req.body.id} HAS deleted!`);
});

router.route("/").put((req, res) => {
    Exercise.findById(req.body.id)
        .then((exercise) => {
            exercise.username = req.body.username;
            exercise.description = req.body.description;
            exercise.duration = Number(req.body.duration);
            exercise.date = Date.parse(req.body.date);

            exercise
                .save()
                .then(() => res.json("Dish washing Record updated!"))
                .catch((err) => res.status(400).json("Error: " + err));
        })
        .catch((err) => res.status(400).json("Error: " + err));
});

module.exports = router;